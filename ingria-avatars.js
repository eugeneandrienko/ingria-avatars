var _STATIC_FOLDER = '/static';

var sys = require('sys'),
    http = require('http'),
    url = require('url'),
    winston = require('winston'),
    fs = require('fs'),
    config = require('./config');

function read_send_file(filename, response) {
  fs.readFile(filename, function(err, contents) {
    if (!err) {
      response.end(contents);
    } else {
      winston.error('Cannot open file: %s', filename);
      winston.error(err);
    }
  });
}

function load_static_file(filename, content_type, response) {
    var filename = __dirname + filename;
    response.writeHead(200, {'Content-Type': content_type});
    read_send_file(filename, response);
  }

var server = http.createServer(function(request, response) {
  var url_parts = url.parse(request.url);
  winston.info('URL %s', url_parts.pathname);

  switch(url_parts.pathname) {
    case '/':
    case '/index':
    case '/index.htm':
    case '/index.html':
      load_static_file('/static/html/index.html', 'text/html', response);
      break;
    default:
      if (url_parts.pathname.indexOf('/css') == 0) {
        load_static_file(_STATIC_FOLDER + url_parts.pathname, 'text/css', response);
      } else if (url_parts.pathname.indexOf('/js') == 0) {
        load_static_file(_STATIC_FOLDER + url_parts.pathname, 'text/javascript', response);
      } else if (url_parts.pathname.indexOf('/fonts') == 0) {
        load_static_file(_STATIC_FOLDER + url_parts.pathname, 'application/font-woff2', response);
      } else {
        winston.warn('404: %s', url_parts.pathname);
        response.writeHead(404, {'Content-Type': 'text/html'});
        response.end('ERROR 404');
      }
      break;
  }
});

winston.remove(winston.transports.Console);
winston.add(winston.transports.Console,
  {
    level: 'info',
    colorize: true,
    timestamp: true,
    prettyPrint: true,
    depth: 10,
    humanReadableUnhandledException: true
  });
winston.add(winston.transports.File,
  {
    level: 'info',
    timestamp: true,
    prettyPrint: true,
    depth: 10,
    filename: config.logfile,
    maxsize: 52428800, // 50 MB logs.
    maxFiles: 20,
    json: false
});

server.listen(8080);
winston.info('Server \'ingria-avatars\' started.');
