# ingria-avatars
Overlay Ingria flag on given image

Based on the next software:
  * Twitter Bootstrap: http://getbootstrap.com/
  * jQuery: https://jquery.com/
  * Node.js: https://nodejs.org/
  * Image Cropper - jQuery image cropping plugin: http://fengyuanchen.github.io/cropper
  * Winston - multi-transport async logging library: https://github.com/winstonjs/winston
